---
layout: post
title: Bye-bye, Facebook!
date: 2020-11-09 22:52 +0100
---

Long time no see, everybody!

I don't really know where else to put this: I have recently deactivated my Facebook
profile. And it feels glorious!

This means I don't see your posts on Facebook or your event invitations. I still use
Messenger but that's temporary and I'd like let go of that rather sooner than later. For
the time being I am still figuring out how to stay in contact and tying up loose ends.[^1]

Why am I writing to you? And how do you know I am writing to *you* specifically? Please
contact me via email, Signal or Threema. For the time being I will also keep WhatsApp
around because I'm not sure I want to be that weird yet, so use that if you must. If you
have the numbers and addresses necessary, I am talking to you.

For the rest of you or if you want to stay connected on the social medias like our
Grandparents do: I remain @bobschi on Twitter. I still have Instagram to post photos as
well but it's heavily courated towards climbing, outdoor stuff, photography, music and
drag queens. 🤷‍♂️

[^1]:
    Why? I felt like I was done. Algorithmic curation of our social streams has brought
    us a species and myself as a person more pain than joy. That, and I couldn't stand the
    asshats on Facebook anymore that seemed to be cropping up more frequently in my feed. I
    felt relieved after pressing the deactivation button which tells me I'm on the right
    track. Maybe I'll change my mind later, maybe I won't, but for the time being I am
    happier without a Facebook profile than with one.

